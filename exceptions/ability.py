from exceptions.custom import CustomException


class AbilityNotUsed(CustomException):
    def __init__(self, msg: str = None):
        if not msg:
            msg = 'Ability not used!'
        super().__init__(msg)
