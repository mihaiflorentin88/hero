from exceptions.custom import CustomException


class FightCompleted(CustomException):
    def __init__(self, msg):
        super().__init__(msg)
