import random
from models.character import Character
from abilities.strategy_client import AbilityStrategyClient


class CharacterProvider:

    names = [
        'Cencan',
        'Wynanne',
        'Grimcwenesc',
        'Clachell',
        'Ape',
        'Donanne',
        'Syli',
        'Vidtri',
        'Paris',
        'Ingstan',
        'Pabert',
        'Ceo',
        'Manthryth',
        'Sali',
        'Wilza',
        'Hamanne',
        'Crowgar',
        'Wig',
        'Cwentri',
        'Isum',
        'Thurinul',
        'Born',
        'El - ner',
        'Amarcír',
        'Rondrenphor',
        'Thornfan',
        'Mocír',
        'Taemel',
        'Edso',
        'Kael',
        'Maldin',
        'Thalfin',
        'Danvar',
        'Var',
        'Leonel',
        'Momo',
        'Rielthor',
        'Gardi',
        'Dáindáin',
        'Dasber',
        'Dornga',
        'Moknút',
        'Chrip',
        'Hanthud',
        'Nugad',
        'Didakmi',
        'Rihrím',
        'Hjalmag',
        'Funma',
        'Thiga',
        'Ragril',
        'Bagi',
        'Tobel',
        'Khagu',
        'Barbar',
    ]

    def __init__(self, abilities: AbilityStrategyClient):
        self.abilities = abilities

    def _generate(self) -> Character:
        return Character(
            name=random.choice(self.names),
            health=random.randint(280, 400),
            strength=random.randint(60, 90),
            defense=random.randint(40, 60),
            speed=random.randint(40, 60),
            luck=random.randint(2, 4),
            abilities=self.abilities
        )

    def generate(self, num: int) -> list[Character]:
        return [self._generate() for _ in range(num)]
