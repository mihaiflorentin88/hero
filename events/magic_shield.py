from logging import Logger
from events.message import FightMessage
from events.subscriber import Subscriber
from abilities.magic_shield import MagicShield


class MagicShieldAbilitySubscriber(Subscriber):
    EVENT_NAME = MagicShield.NAME

    def __init__(self, logger: Logger):
        super().__init__(logger)

    def update(self, message: FightMessage):
        if message.event_name != self.EVENT_NAME:
            return
        self.__publish(message)

    def __publish(self, message: FightMessage):
        attacker, defender = message.attacker, message.defender
        self.logger.warning(
            f"Defender, {defender.name.capitalize()} uses magic shield and heals himself for {message.amount} health "
            f"from {attacker.name.capitalize()}'s attack!"
        )
