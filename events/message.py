from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from models.character import Character


class MessageInterface:
    def __init__(self, event_name: str):
        self.event_name = event_name


class FightMessage(MessageInterface):
    def __init__(self, attacker: 'Character', defender: 'Character', amount: int, event_name: str):
        self.attacker = attacker
        self.defender = defender
        self.amount = amount
        super().__init__(event_name)


class LifeStealMessage(FightMessage):
    def __init__(self, attacker: 'Character', defender: 'Character', amount: int, healed: int, event_name: str):
        super().__init__(attacker, defender, amount, event_name)
        self.healed = healed

