from logging import Logger
from events.message import LifeStealMessage
from events.subscriber import Subscriber
from abilities.life_steal import LifeSteal


class LifeStealAbilitySubscriber(Subscriber):
    EVENT_NAME = LifeSteal.NAME

    def __init__(self, logger: Logger):
        super().__init__(logger)

    def update(self, message: LifeStealMessage):
        if message.event_name != self.EVENT_NAME:
            return
        self.__publish(message)

    def __publish(self, message: LifeStealMessage):
        attacker, defender = message.attacker, message.defender
        self.logger.debug(
            f"Attacker, {attacker.name.capitalize()} strikes "
            f"{defender.name.capitalize()} with life steal for {message.amount} damage!"
        )
        self.logger.warning(
            f"Attacker, {attacker.name.capitalize()} heals himself for {message.healed} health."
        )
