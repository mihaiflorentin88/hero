from logging import Logger
from abilities.thorns import Thorns
from events.message import FightMessage
from events.subscriber import Subscriber


class ThornsAbilitySubscriber(Subscriber):
    EVENT_NAME = Thorns.NAME

    def __init__(self, logger: Logger):
        super().__init__(logger)

    def update(self, message: FightMessage):
        if message.event_name != self.EVENT_NAME:
            return
        self.__publish(message)

    def __publish(self, message: FightMessage):
        attacker, defender = message.attacker, message.defender
        self.logger.debug(
            f"Defender, {defender.name.capitalize()} uses thorns and returns {message.amount} damage"
            f" to {attacker.name.capitalize()}!"
        )
