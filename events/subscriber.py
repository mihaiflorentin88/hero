import abc
from logging import Logger
from events.message import MessageInterface


class Subscriber(abc.ABC):
    def __init__(self, logger: Logger):
        self.logger = logger

    @abc.abstractmethod
    def update(self, message: MessageInterface):
        pass
