from logging import Logger
from abilities.attack import Attack
from events.message import FightMessage
from events.subscriber import Subscriber


class AttackAbilitySubscriber(Subscriber):
    EVENT_NAME = Attack.NAME

    def __init__(self, logger: Logger):
        super().__init__(logger)

    def update(self, message: FightMessage):
        if message.event_name != self.EVENT_NAME:
            return
        self.__publish(message)

    def __publish(self, message: FightMessage):
        attacker, defender = message.attacker, message.defender
        self.logger.debug(
            f"Attacker, {attacker.name.capitalize()} strikes {defender.name.capitalize()} for {message.amount} damage!"
        )
