from logging import Logger
from abilities.defend import Defend
from events.message import FightMessage
from events.subscriber import Subscriber


class DefendAbilitySubscriber(Subscriber):
    EVENT_NAME = Defend.NAME

    def __init__(self, logger: Logger):
        super().__init__(logger)

    def update(self, message: FightMessage):
        if message.event_name != self.EVENT_NAME:
            return
        self.__publish(message)

    def __publish(self, message: FightMessage):
        attacker, defender = message.attacker, message.defender
        self.logger.error(
            f"Defender, {defender.name.capitalize()} takes {message.amount} damage"
            f" from {attacker.name.capitalize()}'s attack!"
        )
