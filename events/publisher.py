from events.subscriber import Subscriber
from events.message import MessageInterface


class Publisher:
    def __init__(self):
        self.__subscribers = set()

    def register(self, subscriber: Subscriber):
        if not isinstance(subscriber, Subscriber):
            raise AttributeError(f'Subscriber is not an instance of "Subscriber" obj. Type {type(subscriber)} given')
        self.__subscribers.add(subscriber)

    def unregister(self, subscriber: Subscriber):
        if not isinstance(subscriber, Subscriber):
            raise AttributeError(f'Subscriber is not an instance of "Subscriber" obj. Type {type(subscriber)} given')
        self.__subscribers.discard(subscriber)

    def dispatch(self, message: MessageInterface):
        if not isinstance(message, MessageInterface):
            raise AttributeError(
                f'The message attribute is not an instance of MessageInterface. Type {type(message)} given'
            )
        for subscriber in self.__subscribers:
            subscriber.update(message)
