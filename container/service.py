import inspect

from config.logging import get_logger

GLOBAL_SERVICES = [
    'config'
]

GLOBAL_INSTANCES = {}


# noinspection PyMethodMayBeStatic
class ServiceContainer(object):
    """
    This module handles the dependency injection within the service.
    """
    def __init__(self, use_mocks: bool = False):
        super(ServiceContainer, self).__init__()
        self.instances_by_name = {'container': self}
        self.use_mocks = use_mocks

    @staticmethod
    def get_service_name(full_name):
        return full_name.split('-')[0]

    def register(self, name, instance):
        if self.get_service_name(name) in GLOBAL_SERVICES:
            GLOBAL_INSTANCES[name] = instance
        else:
            self.instances_by_name[name] = instance
        return self

    def get_instance(self, name):
        instance = GLOBAL_INSTANCES.get(name)
        if instance is not None:
            return instance
        instance = self.instances_by_name.get(name)
        if instance is not None:
            return instance
        factory = self.get_factory(name)
        instance = factory()
        self.register(name, instance)
        return instance

    def get_instances(self, *names):
        result = {}
        for name in names:
            result[name] = self.get_instance(name)
        return result

    def get_kwargs(self, method):
        args = inspect.signature(method)
        kwargs = {}
        for arg in args[0]:
            try:
                kwargs[arg] = self.get_instance(arg)
            except NotImplementedError:
                pass

        return kwargs

    def get_factory(self, name):
        service_name = self.get_service_name(name)
        factory_method = getattr(self, f'make_{service_name}', None)
        if not callable(factory_method):
            raise NotImplementedError("There is no factory for service '%s'." % name)
        return factory_method

    # ------------ EVENTS -------------
    def make_attack_subscriber(self):
        """:return: events.attack.AttackAbilitySubscriber"""
        from events.attack import AttackAbilitySubscriber
        return AttackAbilitySubscriber(get_logger('attack'))

    def get_attack_subscriber(self):
        """:return: events.attack.AttackAbilitySubscriber"""
        return self.get_instance('attack_subscriber')

    def make_life_steal_subscriber(self):
        """:return: events.life_steal.LifeStealAbilitySubscriber"""
        from events.life_steal import LifeStealAbilitySubscriber
        return LifeStealAbilitySubscriber(get_logger('life_steal'))

    def get_life_steal_subscriber(self):
        """:return: events.life_steal.LifeStealAbilitySubscriber"""
        return self.get_instance('life_steal_subscriber')

    def make_rapid_strike_subscriber(self):
        """:return: events.rapid_strike.RapidStrikeAbilitySubscriber"""
        from events.rapid_strike import RapidStrikeAbilitySubscriber
        return RapidStrikeAbilitySubscriber(get_logger('rapid_strike'))

    def get_rapid_strike_subscriber(self):
        """:return: events.rapid_strike.RapidStrikeAbilitySubscriber"""
        return self.get_instance('rapid_strike_subscriber')

    def make_defend_subscriber(self):
        """:return: events.defend.DefendAbilitySubscriber"""
        from events.defend import DefendAbilitySubscriber
        return DefendAbilitySubscriber(get_logger('defend'))

    def get_defend_subscriber(self):
        """:return: events.defend.DefendAbilitySubscriber"""
        return self.get_instance('defend_subscriber')

    def make_thorns_subscriber(self):
        """:return: events.thorns.ThornsAbilitySubscriber"""
        from events.thorns import ThornsAbilitySubscriber
        return ThornsAbilitySubscriber(get_logger('thorns'))

    def get_thorns_subscriber(self):
        """:return: events.thorns.ThornsAbilitySubscriber"""
        return self.get_instance('thorns_subscriber')

    def make_magic_shield_subscriber(self):
        """:return: events.magic_shield.MagicShieldAbilitySubscriber"""
        from events.magic_shield import MagicShieldAbilitySubscriber
        return MagicShieldAbilitySubscriber(get_logger('magic_shield'))

    def get_magic_shield_subscriber(self):
        """:return: events.magic_shield.MagicShieldAbilitySubscriber"""
        return self.get_instance('magic_shield_subscriber')

    def make_publisher(self):
        """:return: events.publisher.Publisher"""
        from events.publisher import Publisher
        publisher = Publisher()
        subscribers = [
            self.get_attack_subscriber(),
            self.get_life_steal_subscriber(),
            self.get_rapid_strike_subscriber(),
            self.get_defend_subscriber(),
            self.get_magic_shield_subscriber(),
            self.get_thorns_subscriber()
        ]
        for subscriber in subscribers:
            publisher.register(subscriber)
        return publisher

    def get_publisher(self):
        """:return: events.publisher.Publisher"""
        return self.get_instance('publisher')

    # ------------ ABILITIES -------------
    def make_attack_ability(self):
        """:return: abilities.attack.Attack"""
        from abilities.attack import Attack
        return Attack(publisher=self.get_publisher())

    def get_attack_ability(self):
        """:return: abilities.attack.Attack"""
        return self.get_instance('attack_ability')

    def make_rapid_strike_ability(self):
        """:return: abilities.rapid_strike.RapidStrike"""
        from abilities.rapid_strike import RapidStrike
        return RapidStrike(publisher=self.get_publisher())

    def get_rapid_strike_ability(self):
        """:return: abilities.rapid_strike.RapidStrike"""
        return self.get_instance('rapid_strike_ability')

    def make_life_steal_ability(self):
        """:return: abilities.life_steal.LifeSteal"""
        from abilities.life_steal import LifeSteal
        return LifeSteal(publisher=self.get_publisher())

    def get_life_steal_ability(self):
        """:return: abilities.life_steal.LifeSteal"""
        return self.get_instance('life_steal_ability')

    def make_defend_ability(self):
        """:return: abilities.defend.Defend"""
        from abilities.defend import Defend
        return Defend(publisher=self.get_publisher())

    def get_defend_ability(self):
        """:return: abilities.defend.Defend"""
        return self.get_instance('defend_ability')

    def make_magic_shield_ability(self):
        """:return: abilities.magic_shield.MagicShield"""
        from abilities.magic_shield import MagicShield
        return MagicShield(publisher=self.get_publisher())

    def get_magic_shield_ability(self):
        """:return: abilities.magic_shield.MagicShield"""
        return self.get_instance('magic_shield_ability')

    def make_thorns_ability(self):
        """:return: abilities.thorns.Thorns"""
        from abilities.thorns import Thorns
        return Thorns(publisher=self.get_publisher())

    def get_thorns_ability(self):
        """:return: abilities.thorns.Thorns"""
        return self.get_instance('thorns_ability')

    def make_offensive_strategy(self):
        """:return: abilities.offensive_strategy.OffensiveStrategy"""
        from abilities.offensive_strategy import OffensiveStrategy
        return OffensiveStrategy(
            attack=self.get_attack_ability(),
            rapid_strike=self.get_rapid_strike_ability(),
            life_steal=self.get_life_steal_ability()
        )

    def get_offensive_strategy(self):
        """:return: abilities.offensive_strategy.OffensiveStrategy"""
        return self.get_instance('offensive_strategy')

    def make_defensive_strategy(self):
        """:return: abilities.defensive_strategy.DefensiveStrategy"""
        from abilities.defensive_strategy import DefensiveStrategy
        return DefensiveStrategy(
            defend=self.get_defend_ability(),
            magic_shield=self.get_magic_shield_ability(),
            thorns=self.get_thorns_ability()
        )

    def get_defensive_strategy(self):
        """:return: abilities.defensive_strategy.OffensiveStrategy"""
        return self.get_instance('defensive_strategy')

    def make_ability_strategy_client(self):
        """:return: abilities.strategy_client.AbilityStrategyClient"""
        from abilities.strategy_client import AbilityStrategyClient
        return AbilityStrategyClient(
           offensive=self.get_offensive_strategy(),
           defensive=self.get_defensive_strategy()
        )

    def get_ability_strategy_client(self):
        """:return: abilities.strategy_client.AbilityStrategyClient"""
        return self.get_instance('ability_strategy_client')

    # ----- PROVIDERS -----

    def make_character_provider(self):
        """:return providers.character.CharacterProvider"""
        from providers.character import CharacterProvider
        return CharacterProvider(abilities=self.get_ability_strategy_client())

    def get_character_provider(self):
        """:return providers.character.CharacterProvider"""
        return self.get_instance('character_provider')

