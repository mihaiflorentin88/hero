import random

from typing import Optional
from abilities.strategy_client import AbilityStrategyClient


class Character:
    def __init__(self,
                 name: str,
                 health: int,
                 strength: int,
                 defense: int,
                 speed: int,
                 luck: int,
                 abilities: AbilityStrategyClient
                 ):
        self.name: str = name
        self.health: int = health
        self.strength: int = strength
        self.defense: int = defense
        self.speed: int = speed
        self.luck: int = luck
        self.abilities = abilities
        self.defended_log = []
        self.attack_log = []

    def __str__(self):
        return f"""
             {self.name}
        {'-' * 20}
        health: {self.health}
        strength: {self.strength}
        defense: {self.defense}
        speed: {self.speed}
        luck: {self.luck}
        """

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, name: Optional[str]):
        if not name:
            name = input('Please enter a name for this character: ')
        self._name = name

    @property
    def health(self) -> int:
        return self._health

    @health.setter
    def health(self, health: Optional[int]):
        if not health:
            health = random.randint(100, 200)
        self._health = health

    @property
    def strength(self) -> int:
        return self._strength

    @strength.setter
    def strength(self, strength: Optional[int]):
        if not strength:
            strength = random.randint(60, 90)
        self._strength = strength

    @property
    def defense(self) -> int:
        return self._defense

    @defense.setter
    def defense(self, defense: Optional[int]):
        if not defense:
            defense = random.randint(40, 60)
        self._defense = defense

    @property
    def speed(self) -> int:
        return self._speed

    @speed.setter
    def speed(self, speed: Optional[int]):
        if not speed:
            speed = random.randint(40, 60)
        self._speed = speed

    @property
    def luck(self) -> int:
        return self._luck

    @luck.setter
    def luck(self, luck: Optional[int]):
        if not luck:
            luck = random.randint(2, 4)
        self._luck = luck

    def attack(self, character: 'Character') -> int:
        dmg_done = self.abilities.use(attacker=self, defender=character, strategy=AbilityStrategyClient.TYPE_OFFENSIVE)
        self.attack_log.append(dmg_done)
        return dmg_done

    def defend(self, attacker: 'Character') -> int:
        dmg_taken = self.abilities.use(attacker=attacker, defender=self, strategy=AbilityStrategyClient.TYPE_DEFENSIVE)
        self.defended_log.append(dmg_taken)
        return dmg_taken
