import logging


class ColorFormatter(logging.Formatter):

    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    blue = "\x1b[34;20m"
    green = "\x1b[32;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    frmt = '%(name)s: %(message)s'
    # fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"

    def __init__(self):
        super(ColorFormatter, self).__init__()

    FORMATS = {
        logging.DEBUG: blue + frmt + reset,
        logging.INFO: grey + frmt + reset,
        logging.WARNING: green + frmt + reset,
        logging.ERROR: red + frmt + reset,
        logging.CRITICAL: bold_red + frmt + reset
    }

    def format(self, record: logging.LogRecord) -> str:
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


def get_logger(name: str) -> logging.Logger:
    _logger = logging.getLogger(name)
    color_formatter = ColorFormatter()
    _logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(color_formatter)
    _logger.addHandler(handler)
    return _logger
