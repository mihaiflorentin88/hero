import exceptions

from config.logging import get_logger
from models.character import Character
from container.service import ServiceContainer


def fight(attacker: Character, defender: Character) -> tuple[Character, Character]:
    try:
        attacker.attack(defender)
    except exceptions.FightCompleted as e:
        logger.info(e)
        winner, loser = (attacker, defender) if attacker.health > 0 else (defender, attacker)
        return winner, loser
    return fight(defender, attacker)


def decide_attacker(hero1: Character, hero2: Character) -> tuple[Character, Character]:
    fighters = [hero1, hero2]
    sort_property = 'speed' if hero1.speed != hero2.speed else 'luck'
    fighters.sort(key=lambda x: getattr(x, sort_property), reverse=True)
    attacker, defender = fighters
    return attacker, defender


def initiate(heroes: list[Character]):
    if len(heroes) == 1:
        logger.info(f"Battle concluded:\nWinner:\n{str(heroes[0])}")
        exit(0)
    attacker, defender = decide_attacker(heroes[0], heroes[1])
    logger.info('-' * 90)
    logger.info(f'Starting fight between\nAttacker:\n{str(attacker)}\nDefender:\n{defender}')
    winner, loser = fight(attacker, defender)
    logger.info('-' * 90)
    heroes.remove(loser)
    initiate(heroes)


def main():
    provider = container.get_character_provider()
    heroes = provider.generate(num_characters)
    logger.info('Heroes:\n')
    for hero in heroes:
        logger.info(str(hero))
    initiate(heroes)


if __name__ == '__main__':
    container = ServiceContainer()
    logger = get_logger(' ')
    num_characters = int(input('How many heroes do you want to participate in the battle ? '))
    if num_characters <= 1:
        raise Exception('At least 2 heroes are required for the battle to begin.')
    main()
