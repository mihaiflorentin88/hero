import exceptions

from abilities.ability import Ability
from events.publisher import Publisher
from typing import Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from models.character import Character


class RapidStrike(Ability):
    NAME = 'rapid_strike'

    def __init__(self, publisher: Publisher, proc_chance: Optional[int] = 2):
        self._min_proc_chance = 1
        self._max_proc_chance = 10
        super().__init__(publisher, self.NAME, proc_chance)

    def _use(self, attacker: 'Character', defender: 'Character'):
        """
        Rapid strike: Strikes twice.
        There’s a n% (default = 10%) chance the attacker will use this skill every time he attacks
        """
        dmg = [attacker.attack(defender) for _ in range(2)] if self._has_procced() else False
        if not dmg:
            raise exceptions.AbilityNotUsed()
        return sum(dmg)
