import random
import exceptions

from typing import TYPE_CHECKING
from abilities.defend import Defend
from abilities.ability import Ability
from abilities.thorns import Thorns
from abilities.magic_shield import MagicShield
from abilities.strategy import AbilityStrategy

if TYPE_CHECKING:
    from models.character import Character


class DefensiveStrategy(AbilityStrategy):
    def __init__(self, defend: Defend, magic_shield: MagicShield, thorns: Thorns):
        self.defend = defend
        self.magic_shield = magic_shield
        self.thorns = thorns

    def use(self, attacker: 'Character', defender: 'Character') -> int:
        if defender.health <= 0:
            raise exceptions.FightCompleted(f'Fight completed! {attacker.name.capitalize()} won!')
        ability: Ability = random.choice([self.magic_shield, self.thorns])
        dmg_taken = self.defend.use(attacker, defender)
        try:
            ability.use(attacker, defender)
            if attacker.health <= 0:
                raise exceptions.FightCompleted(f'Fight completed! {defender.name.capitalize()} won!')
        except exceptions.AbilityNotUsed:
            pass  # do nothing
        return dmg_taken
