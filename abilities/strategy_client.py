from typing import TYPE_CHECKING
from abilities.offensive_strategy import OffensiveStrategy
from abilities.defensive_strategy import DefensiveStrategy

if TYPE_CHECKING:
    from models.character import Character


class AbilityStrategyClient:
    TYPE_OFFENSIVE = 'offensive'
    TYPE_DEFENSIVE = 'defensive'

    def __init__(self, offensive: OffensiveStrategy, defensive: DefensiveStrategy):
        self.offensive = offensive
        self.defensive = defensive

    def use(self, attacker: 'Character', defender: 'Character', strategy: str) -> int:
        if strategy == self.TYPE_OFFENSIVE:
            return self.offensive.use(attacker, defender)
        if strategy == self.TYPE_DEFENSIVE:
            return self.defensive.use(attacker, defender)
        raise Exception(f'Invalid strategy given `{strategy}`.')
