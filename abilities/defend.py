from abilities.ability import Ability
from events.publisher import Publisher
from typing import Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from models.character import Character


class Defend(Ability):
    NAME = 'defend'

    def __init__(self, publisher: Publisher, proc_chance: Optional[int] = None):
        super().__init__(publisher, self.NAME, None)

    def _use(self, attacker: 'Character', defender: 'Character') -> int:
        dmg_taken = attacker.strength - defender.defense
        defender.health -= dmg_taken
        defender.defended_log.append(dmg_taken)
        return dmg_taken
