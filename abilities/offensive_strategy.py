import random
import exceptions

from typing import TYPE_CHECKING
from abilities.attack import Attack
from abilities.ability import Ability
from abilities.life_steal import LifeSteal
from abilities.rapid_strike import RapidStrike
from abilities.strategy import AbilityStrategy

if TYPE_CHECKING:
    from models.character import Character


class OffensiveStrategy(AbilityStrategy):
    def __init__(self, attack: Attack, rapid_strike: RapidStrike, life_steal: LifeSteal):
        self.attack = attack
        self.rapid_strike = rapid_strike
        self.life_steal = life_steal

    def use(self, attacker: 'Character', defender: 'Character') -> int:
        ability: Ability = random.choice([self.rapid_strike, self.life_steal])
        try:
            return ability.use(attacker, defender)
        except exceptions.AbilityNotUsed:
            return self.attack.use(attacker, defender)
