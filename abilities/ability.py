import random

from abc import ABC, abstractmethod
from events.publisher import Publisher
from events.message import FightMessage
from typing import Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from models.character import Character


class Ability(ABC):
    _min_proc_chance = 1
    _max_proc_chance = 10

    def __init__(self, publisher: Publisher, name: str, proc_chance: Optional[int]):
        self.name = name
        self.publisher = publisher
        self.proc_chance = proc_chance or self._min_proc_chance

    @property
    def proc_chance(self) -> int:
        return self._proc_chance

    @proc_chance.setter
    def proc_chance(self, chance: Optional[int]):
        if not chance:
            chance = 1
        if chance > self._max_proc_chance or chance < self._min_proc_chance:
            raise Exception(
                f'Value for proc_chance can only be between {self._min_proc_chance} and {self._max_proc_chance}.'
            )
        self._proc_chance = chance

    def _has_procced(self) -> bool:
        return self.proc_chance >= random.randint(self._min_proc_chance, self._max_proc_chance)

    def use(self, attacker: 'Character', defender: 'Character') -> int:
        dmg = self._use(attacker, defender)
        self._dispatch(attacker, defender, dmg)
        return dmg

    def _dispatch(self, attacker: 'Character', defender: 'Character', dmg: int):
        message = FightMessage(
            event_name=self.name,
            attacker=attacker,
            defender=defender,
            amount=dmg,
        )
        self.publisher.dispatch(message)

    @abstractmethod
    def _use(self, attacker: 'Character', defender: 'Character') -> int:
        pass
