import exceptions
from abilities.ability import Ability
from events.publisher import Publisher
from typing import Optional, TYPE_CHECKING
from events.message import LifeStealMessage

if TYPE_CHECKING:
    from models.character import Character


class LifeSteal(Ability):
    NAME = 'life_steal'

    def __init__(self, publisher: Publisher, proc_chance: Optional[int] = 1):
        self._min_proc_chance = 1
        self._max_proc_chance = 10
        super().__init__(publisher, self.NAME, proc_chance)

    def use(self, attacker: 'Character', defender: 'Character') -> int:
        return self._use(attacker, defender)

    def _use(self, attacker: 'Character', defender: 'Character') -> int:
        import math
        """
        Life Steal: Heals for 20% of the damage done when procced.
        There’s a n% (default = 10%) chance the attacker will use this skill every time he attacks
        """
        dmg = attacker.attack(defender) if self._has_procced() else False
        if not dmg:
            raise exceptions.AbilityNotUsed()
        healed = math.ceil(dmg * 0.2)
        attacker.health += math.ceil(dmg * 0.2)
        self.__dispatch(attacker, defender, dmg, healed)
        return dmg

    def __dispatch(self, attacker: 'Character', defender: 'Character', dmg: int, heal: int):
        message = LifeStealMessage(
            event_name=str(self),
            attacker=attacker,
            defender=defender,
            amount=dmg,
            healed=heal
        )
        self.publisher.dispatch(message)
