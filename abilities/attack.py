from typing import TYPE_CHECKING
from abilities.ability import Ability
from events.publisher import Publisher

if TYPE_CHECKING:
    from models.character import Character


class Attack(Ability):
    NAME = 'attack'

    def __init__(self, publisher: Publisher):
        super().__init__(publisher, self.NAME, None)

    def _use(self, attacker: 'Character', defender: 'Character') -> int:
        dmg_done = defender.defend(attacker)
        attacker.attack_log.append(dmg_done)
        return dmg_done
