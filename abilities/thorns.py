import exceptions
from abilities.ability import Ability
from events.publisher import Publisher
from typing import Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from models.character import Character


class Thorns(Ability):
    NAME = 'thorns'

    def __init__(self, publisher: Publisher, proc_chance: Optional[int] = 3):
        self._min_proc_chance = 2
        self._max_proc_chance = 10
        super().__init__(publisher, self.NAME, proc_chance)

    def _use(self, attacker: 'Character', defender: 'Character') -> int:
        import math
        """
        Thorns: Causes attackers to take 50% of the damage done when they deal damage to the defender.
        There’s a n% (default = 30%) chance the defender will use this skill every time he takes damage.
        """
        dmg = defender.defended_log[-1] if self._has_procced() else False
        if not dmg:
            raise exceptions.AbilityNotUsed()
        return_dmg = math.ceil(dmg * 0.5)
        attacker.health -= return_dmg
        return return_dmg
