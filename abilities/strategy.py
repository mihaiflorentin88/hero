from typing import TYPE_CHECKING
from abc import ABC, abstractmethod

if TYPE_CHECKING:
    from models.character import Character


class AbilityStrategy(ABC):
    @abstractmethod
    def use(self, attacker: 'Character', defender: 'Character') -> int:
        pass
